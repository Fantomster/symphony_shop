<?php
	/**
	 * Created by PhpStorm.
	 * User: Konstantin Silukov
	 * Date: 29.01.2018
	 * Time: 17:29
	 */
	$var = 1 <=> 2;
	echo $var;

	$object = new class {
		public function hello($message){
			return "Hello $message";
		}
	};

	echo $object->hello('PHP');

	var_dump((bool)false);
	var_dump(is_bool((bool)false));
	var_dump((bool)'false');
	var_dump(is_bool((bool)'false'));
exit();
//	class Orders implements Iterator{
//		/**
//		 * Return the current element
//		 * @link http://php.net/manual/en/iterator.current.php
//		 * @return mixed Can return any type.
//		 * @since 5.0.0
//		 */
//		public function current(){
//
//		}
//
//		/**
//		 * Move forward to next element
//		 * @link http://php.net/manual/en/iterator.next.php
//		 * @return void Any returned value is ignored.
//		 * @since 5.0.0
//		 */
//		public function next(){
//
//		}
//
//		/**
//		 * Return the key of the current element
//		 * @link http://php.net/manual/en/iterator.key.php
//		 * @return mixed scalar on success, or null on failure.
//		 * @since 5.0.0
//		 */
//		public function key(){
//
//		}
//
//		/**
//		 * Checks if current position is valid
//		 * @link http://php.net/manual/en/iterator.valid.php
//		 * @return boolean The return value will be casted to boolean and then evaluated.
//		 * Returns true on success or false on failure.
//		 * @since 5.0.0
//		 */
//		public function valid(){
//
//		}
//
//		/**
//		 * Rewind the Iterator to the first element
//		 * @link http://php.net/manual/en/iterator.rewind.php
//		 * @return void Any returned value is ignored.
//		 * @since 5.0.0
//		 */
//		public function rewind(){
//
//		}
//	}


/*	class A {

		public function __construct(){

		}

		public static function D_twelwe(){
			return function (){
				return !1;
			};
		}
	}

	var_dump((bool)A::D_twelwe()());
*/

/*
class Customer{
	public function __construct(){
		echo '__construct';
	}

	public function __destruct(){
		echo '__destruct';
	}

	public function __toString(){
		echo '__toString';
		return '__toString';
	}

	public function __call($name, $arguments){
		echo '__call';
	}
}

$customer = new Customer();

$s = serialize($customer);

$u = unserialize($s);
echo  "<br>" . get_class($u) . "<br>";

$u = unserialize($s, ['allowed_classes' => false]);
echo get_class($u) . "<br>";
*/

list($color1, $color2, $color3) = ['green', 'yellow', 'blue'];
var_dump($color1, $color2, $color3);

list($colors[], $colors[], $colors[]) = ['green', 'yellow', 'blue'];
var_dump($colors);
