<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 05.10.2017
 * Time: 21:12
 */

namespace FF\ShipmentBundle\Service;


class FlatRateShipment
{
	private $router;

	public function __construct(
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->router = $router;
	}

	public function getInfo($street, $city, $country, $postcode, $amount, $qty){
		return array(
			'shipment' => array(
				'title' => 'FF FlatRate Shipment',
				'code' => 'flat_rate',
				'delivery_options' => array(
					'title' => 'Fixed',
					'code' => 'fixed',
					'price' => 9.99
				),
				'url_process' => $this->router->generate('ff_shipment_flat_rate_process'),
			)
		);
	}
}