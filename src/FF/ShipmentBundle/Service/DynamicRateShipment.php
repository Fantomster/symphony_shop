<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 05.10.2017
 * Time: 23:06
 */

namespace FF\ShipmentBundle\Service;


class DynamicRateShipment
{
	private $router;

	public function __construct(
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->router = $router;
	}

	public function getInfo($street, $city, $country, $postcode, $amount, $qty){
		return array(
			'shipment' => array(
				'title' => 'FF DynamicRate Shipment',
				'code' => 'dynamic_rate_shipment',
				'delivery_options' => $this->getDeliveryOptions($street, $city, $country, $postcode, $amount, $qty),
				'url_process' => $this->router->generate('ff_shipment_dynamic_rate_process'),
			)
		);
	}

	public function getDeliveryOptions($street, $city, $country, $postcode, $amount, $qty){
		//Imagene we are hitting the API with: $street, $city, $country, $postcode, $amount, $qty
		return array(
			array(
				'title' => 'Same day delivery',
				'code' => 'dynamic_rate_sdd',
				'price' => 9.99
			),
			array(
				'title' => 'Standard delivery',
				'code' => 'dynamic_rate_sd',
				'price' => 4.99
			)
		);
	}
}