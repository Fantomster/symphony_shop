<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 05.10.2017
 * Time: 23:20
 */

namespace FF\ShipmentBundle\Controller;

//use FF\ShipmentBundle\Entity\DynamicRate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DynamicRateController extends Controller
{
	public function processAction(Request $request){
		//Just a dummy string, simulating some transaction id
		$transaction = md5(time() . uniqid());

		if($transaction){
			return new JsonResponse(array(
				'success' => $transaction
			));
		}
		return new JsonResponse(array(
			'error' => 'Error occurred while processing DynamicRate shipment.'
		));
	}
}