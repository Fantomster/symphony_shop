<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 05.10.2017
 * Time: 22:27
 */

namespace FF\ShipmentBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FlatRateController extends Controller
{
	public function processAction(Request $request){
		//$imulating some transaction id, if any
		$transaction = md5(time() . uniqid());

		return new JsonResponse(array(
			'success' => $transaction
		));
	}
}