<?php
	/**
	 * Created by PhpStorm.
	 * User: Konstantin Silukov
	 * Date: 24.02.2018
	 * Time: 21:03
	 */

	namespace FF\SalesBundle\Interfacec;

	interface Payment {
		function authorize();
		function capture();
		function cancel();
	}