<?php

namespace FF\SalesBundle;

use FF\SalesBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FFSalesBundle extends Bundle
{
	public function build(ContainerBuilder $container){
		parent::build($container);
		$container->addCompilerPass(new OverrideServiceCompilerPass());
	}
	
}