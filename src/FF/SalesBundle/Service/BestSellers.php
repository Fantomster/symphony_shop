<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 18.10.2017
 * Time: 11:00
 */

namespace FF\SalesBundle\Service;


class BestSellers
{
	private $em;
	private $router;
	
	public function __construct(
		\Doctrine\ORM\EntityManager $entityManager,
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->em = $entityManager;
		$this->router = $router;
	}
	
	public function getItems(){
		$products = array();
		$salesOrderItem = $this->em->getRepository('FFSalesBundle:SalesOrderItem');
		$_products = $salesOrderItem->getBestsellers();
		
		foreach($_products as $_product){
			$products[] = array(
				'path' => $this->router->generate('product_show', array('id' => $_product->getId())),
				'name' => $_product->getTitle(),
				'img' => $_product->getImage(),
				'price' => $_product->getPrice(),
				'id' => $_product->getId(),
			);
		}
		return $products;
	}
}