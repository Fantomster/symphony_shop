<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 17.10.2017
 * Time: 14:30
 */

namespace FF\SalesBundle\Service;


use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CustomerOrders
{
	private $em;
	private $token;
	private $router;
	
	public function __construct(
		\Doctrine\ORM\EntityManager $entityManager,
		TokenStorage $tokenStorage,
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->em = $entityManager;
		$this->token = $tokenStorage->getToken();
		$this->router = $router;
	}
	
	public function getOrders(){
		$orders = array();
		
		if($this->token && $this->token->getUser() instanceof \FF\CustomerBundle\Entity\Customer){
			$salesOrders = $this->em->getRepository('FFSalesBundle:SalesOrder')->findBy(array('customer' => $this->token->getUser()));
			foreach($salesOrders as $salesOrder){
				$orders[] = array(
					'id' => $salesOrder->getId(),
					'date' => $salesOrder->getCreatedAt()->format('d/m/Y H:i:s'),
					'ship_to' => $salesOrder->getAddressFirstName() . '' . $salesOrder->getAddressLastName(),
					'order_total' => $salesOrder->getTotalPrice(),
					'status' => $salesOrder->getStatus(),
					'actions' => array(
						array(
							'label' => 'Cancel',
							'path' => $this->router->generate('ff_sales_order_cancel', array('id' => $salesOrder->getId()))
						),
						array(
							'label' => 'Print',
							'path' => $this->router->generate('ff_sales_order_print', array('id' => $salesOrder->getId()))
						)
					)
				);
			}
		}
		return $orders;
	}
}