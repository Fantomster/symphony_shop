<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 17.10.2017
 * Time: 10:48
 */

namespace FF\SalesBundle\Service;

use FF\CustomerBundle\Entity\Customer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CheckoutMenu
{
	private $em;
	private $token;
	private $router;
	
	public function __construct(
		\Doctrine\ORM\EntityManager $entityManager,
		TokenStorage $tokenStorage,
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->em = $entityManager;
		$this->token = $tokenStorage->getToken();
		$this->router = $router;
	}
	
	public function getItems(){
//		return null; @TODO: maybe bug in this place
			if($this->token->getUser() instanceof Customer){
				$customer = $this->token->getUser();
				$cart = $this->em->getRepository('FFSalesBundle:Cart')->findOneBy(array('customer' => $customer));

				if($cart){
					return array(
						array('path' => $this->router->generate('ff_sales_cart'), 'label' => sprintf('Cart (%s)', count($cart->getItems()))),
						array('path' => $this->router->generate('ff_sales_checkout'),
							'label' => 'Checkout'),
					);
				}
			}
		return array();
	}
}