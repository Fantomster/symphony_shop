<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 16.10.2017
 * Time: 14:40
 */

namespace FF\SalesBundle\Service;


class AddToCartUrl
{
	private $em;
	private $router;
	
	public function __construct(
		\Doctrine\ORM\EntityManager $entityManager,
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->em = $entityManager;
		$this->router = $router;
	}
	
	public function getAddToCartUrl($productId){
			return $this->router->generate('ff_sales_cart_add', array('id' => $productId));
	}
	
	
}