<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 16.10.2017
 * Time: 14:56
 */

namespace FF\SalesBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container){
		//Override 'add_to_cart_url' service
		$container->removeDefinition('add_to_cart_url');
		$container->setDefinition('add_to_cart_url', $container->getDefinition('ff_sales.add_to_cart_url'));
		
		$container->removeDefinition('checkout_menu');
		$container->setDefinition('checkout_menu', $container->getDefinition('ff_sales.checkout_menu'));
		
		$container->removeDefinition('ff_customer.customer_orders');
		$container->setDefinition('ff_customer.customer_orders', $container->getDefinition('ff_sales.customer_orders'));
		
		$container->removeDefinition('bestsellers');
		$container->setDefinition('bestsellers', $container->getDefinition('ff_sales.bestsellers'));

		$container->getDefinition('ff_sales.payment')->addArgument(array_keys($container->findTaggedServiceIds('payment_method'))
		);
		
		$container->getDefinition('ff_sales.shipment')->addArgument(array_keys($container->findTaggedServiceIds('shipment_method'))
		);

	}
}