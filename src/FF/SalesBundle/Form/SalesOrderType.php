<?php

namespace FF\SalesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalesOrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('customer')->add('itemsPrice')->add('shipmentPrice')->add('totalPrice')->add('status')->add('paymentMethod')->add('shipmentMethod')->add('createdAt')->add('modifiedAt')->add('customerEmail')->add('customerFirstName')->add('customerLastName')->add('addressFirstName')->add('addressLastName')->add('addressCountry')->add('addressState')->add('addressCity')->add('addressPostcode')->add('addressStreet')->add('addressTelephone');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FF\SalesBundle\Entity\SalesOrder'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ff_salesbundle_salesorder';
    }


}
