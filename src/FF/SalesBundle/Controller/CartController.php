<?php

namespace FF\SalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CartController extends Controller
{
    public function indexAction()
    {
		if($customer = $this->getUser()){
			$em = $this->getDoctrine()->getManager();
			
			$cart = $em->getRepository('FFSalesBundle:Cart')->findOneBy(array('customer' => $customer));
			$items = $cart->getItems();
			$total = null;

			foreach ($items as $item){
				$total += floatval($item->getQty() * $item->getUnitPrice());
			}

			return $this->render('FFSalesBundle:cart:index.html.twig', array(
				'customer' => $customer,
				'items' => $items,
				'total' => $total,
			));
		} else {
			$this->addFlash('warning', 'Only logged in customers can access cart page.');
			return $this->redirectToRoute('ff_customer_login');
		}
    }
    
    public function addAction($id){
    	if($customer = $this->getUser()->getId()){
    		$em = $this->getDoctrine()->getManager();
    		$now = new \DateTime();

    		$product = $em->getRepository('FFCatalogBundle:Product')->find($id);
    		
    		//grab the cart for current user
		    $cart = $em->getRepository('FFSalesBundle:Cart')->findOneBy(array('customer'=> $customer));

		    //If there is no cart, create one
		    if(!$cart){
		    	$cart = new \FF\SalesBundle\Entity\Cart();
		    	$cart->setCustomer($customer);
		    	$cart->setCreatedAt($now);
		    	$cart->setModifiedAt($now);
		    } else {
		    	$cart->setModifiedAt($now);
		    }

		    $em->persist($cart);
		    $em->flush();

		    //Grab the possibly existing cart item
		    //But, lets find it directory
		    $cartItem = $em->getRepository('FFSalesBundle:CartItem')->findOneBy(array('cart'=>$cart, 'product'=>$product));

		    if($cartItem){
		    	//Cart item exists, update it
			    $cartItem->setQty($cartItem->getQty() + 1);
			    $cartItem->setModifiedAt($now);
		    }else{
		    	//Cart item does not exist, add new one
			    $cartItem = new \FF\SalesBundle\Entity\CartItem();
			    $cartItem->setCart($cart);
			    $cartItem->setProduct($product);
			    $cartItem->setQty(1);
			    $cartItem->setUnitPrice($product->getPrice());
			    $cartItem->setCreatedAt($now);
			    $cartItem->setModifiedAt($now);
		    }

		    $em->persist($cartItem);
		    $em->flush();

		    $this->addFlash('success', sprintf('%s successfully added to cart', $product->getTitle()));

		    return $this->redirectToRoute('ff_sales_cart');
	    } else {
    		$this->addFlash('warning', 'Only logged in users can add to cart');
    		return $this->redirect('/');
	    }
    }
    
    public function updateAction(\Symfony\Component\HttpFoundation\Request $request){
    	$items = $request->get('item');

    	$em = $this->getDoctrine()->getManager();
    	foreach($items as $_id => $_qty){
//    		$_id = str_replace('item', '', $_id);
//    		$cartItem = $em->getRepository('FFSalesBundle:CartItem')->findOneBy(array('product' => $_id));
    		$cartItem = $em->getRepository('FFSalesBundle:CartItem')->find($_id);

    		if(intval($_qty) > 0){
    			$cartItem->setQty($_qty);
    			$em->persist($cartItem);
		    }else{
    			$em->remove($cartItem);
		    }
	    }
	    //Persist to database
	    $em->flush();
	    
	    $this->addFlash('success', 'Cart updated.');
	    
	    return $this->redirectToRoute('ff_sales_cart');
    }
}
