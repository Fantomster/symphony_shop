<?php

namespace FF\SalesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FFSalesBundle:Default:index.html.twig');
    }
}
