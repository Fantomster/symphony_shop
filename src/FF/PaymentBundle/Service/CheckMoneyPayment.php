<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 03.10.2017
 * Time: 1:10
 */

namespace FF\PaymentBundle\Service;


class CheckMoneyPayment
{
	private $router;

	public function __construct(
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	){
		$this->router = $router;
	}

	public function getInfo(){
		return array(
			'payment' => array(
				'title' => 'FF Check Money Payment',
				'code' => 'check money',
				'url_authorize' => $this->router->generate('ff_payment_check_money_authorize'),
				'url_capture' => $this->router->generate('ff_payment_check_money_capture'),
				'url_cancel' => $this->router->generate('ff_payment_check_money_cancel'),
				//'form' => ''
			)
		);
	}
}