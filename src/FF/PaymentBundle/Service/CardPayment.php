<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 03.10.2017
 * Time: 0:41
 */

namespace FF\PaymentBundle\Service;

use FF\PaymentBundle\Entity\Card;
use Symfony\Component\Form\FormFactory;

class CardPayment
{
	private $formFactory;
	private $router;

	public function __construct(
		FormFactory $formFactory,
		\Symfony\Bundle\FrameworkBundle\Routing\Router $router
	)
	{
		$this->formFactory = $formFactory;
		$this->router = $router;
	}

	public function getInfo(){
		$card = new Card();
		$form = $this->formFactory->create('FF\PaymentBundle\Form\CardType', $card);

		return array(
			'payment' => array(
				'title' => 'FF Card Payment',
				'code' => 'card_payment',
				'url_authorize' => $this->router->generate('ff_payment_card_authorize'),
				'url_capture' => $this->router->generate('ff_payment_card_capture'),
				'url_cancel' => $this->router->generate('ff_payment_card_cancel'),
				'form' => $form->createView()
			)
		);
	}
}