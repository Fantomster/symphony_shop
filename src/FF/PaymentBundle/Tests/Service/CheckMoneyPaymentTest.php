<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 03.10.2017
 * Time: 14:36
 */

namespace FF\PaymentBundle\Tests\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CheckMoneyPaymentTest extends KernelTestCase
{
	private $container;
	private $router;
	
	public function setUp(){
		static::bootKernel();
		$this->container = static::$kernel->getContainer();
		$this->router = $this->container->get('router');
	}
	
	public function testGetViaService(){
		$payment = $this->container->get('ff_payment.check_money');
		$info = $payment->getInfo();
		$this->assertNotEmpty($info);
	}
	
	public function testGetInfoViaClass(){
		$payment = new \FF\PaymentBundle\Service\CheckMoneyPayment($this->router);
		
		$info = $payment->getInfo();
		$this->assertNotEmpty($info);
	}
}