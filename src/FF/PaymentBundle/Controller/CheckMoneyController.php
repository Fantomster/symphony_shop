<?php

namespace FF\PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CheckMoneyController extends Controller
{
    public function authorizeAction(Request $request){
    	$transaction = md5(time() . uniqid());
    	return new JsonResponse(array(
    		'success' => $transaction
	    ));
    }

    public function captureAction(Request $request){
    	$transaction = md5(time() . uniqid());
    	return new JsonResponse(array(
    		'success' => $transaction
	    ));
    }

    public function cancelAction(Request $request){
    	$transaction = md5(time() . uniqid());
    	return new JsonResponse(array(
    		'success' => $transaction
	    ));
    }
}
