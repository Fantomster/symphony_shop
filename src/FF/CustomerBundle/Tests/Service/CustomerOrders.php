<?php
/**
 * Created by PhpStorm.
 * User: administrator
 * Date: 30.09.2017
 * Time: 17:27
 */

namespace FF\CustomerBundle\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CustomerOrders extends KernelTestCase
{
	private $container;

	public function setUp(){
		static::bootKernel();
		$this->container = static::$kernel->getContainer();
	}

	public function testGetItemsViaService(){
		$orders = $this->container->get('ff_customer.customer_orders');
		$this->assertNotEmpty($orders->getOrders());
	}

	public function testGetItemsViaClass(){
		$orders = new \FF\CustomerBundle\Service\CustomerOrders();
		$this->assertNotEmpty($orders->getOrders());
	}
}