<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 15.09.2017
 * Time: 17:50
 */

namespace FF\CustomerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		//Override the core module 'onsale' service
		$container->removeDefinition('customer_menu');
		$container->setDefinition('customer_menu', $container->getDefinition('ff_customer.customer_menu'));
	}
}