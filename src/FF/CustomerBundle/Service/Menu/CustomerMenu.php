<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 15.09.2017
 * Time: 17:39
 */

namespace FF\CustomerBundle\Service\Menu;

use FF\CustomerBundle\Entity\Customer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class CustomerMenu
{
	private $token;
	private $router;
	
	public function __construct(TokenStorage $tokenStorage, Router $router)
	{
		$this->token = $tokenStorage->getToken();
		$this->router = $router;
	}
	
	public function getItems(){
		$items = array();
		$user = $this->token->getUser();

		if($user instanceof Customer){
			//customer authentication
			$items[] = array(
				'path' => $this->router->generate('customer_account'),
				'label' => $user->getFirstName() . ' ' . $user->getLastName(),
			);
			$items[] = array(
				'path' => $this->router->generate('customer_logout'),
				'label' => 'Logout',
			);
		} else {
			$items[] = array(
				'path' => $this->router->generate('ff_customer_login'),
				'label' => 'Login',
			);
			$items[] = array(
				'path' => $this->router->generate('ff_customer_register'),
				'label' => 'Register',
			);
		}
		
		return $items;
	}
}