<?php

namespace FF\CustomerBundle;

use FF\CustomerBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FFCustomerBundle extends Bundle
{
	public function build(ContainerBuilder $container){
		parent::build($container);
		$container->addCompilerPass(new OverrideServiceCompilerPass());
	}
}