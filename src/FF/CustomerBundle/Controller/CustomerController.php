<?php

	namespace FF\CustomerBundle\Controller;

	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Component\Form\Extension\Core\Type\EmailType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\HttpFoundation\Request;
	use FF\CustomerBundle\Entity\Customer;
	use FF\CustomerBundle\Form\CustomerType;
	use Symfony\Component\Validator\Constraints\Email;

	/**
	 * Customer controller.
	 *
	 * @Route("customer")
	 */
	class CustomerController extends Controller {
		/**
		 * Lists all customer entities.
		 *
		 * @Route("/", name="customer_index")
		 * @Method("GET")
		 */
		public function indexAction(){
			$em = $this->getDoctrine()->getManager();

			$customers = $em->getRepository('FFCustomerBundle:Customer')->findAll();

			return $this->render('FFCustomerBundle:customer:index.html.twig', array(
				'customers' => $customers,
			));
		}

		/**
		 * Creates a new customer entity.
		 *
		 * @Route("/new", name="customer_new")
		 * @Method({"GET", "POST"})
		 */
		public function newAction(Request $request){
			$customer = new Customer();
			$form = $this->createForm('FF\CustomerBundle\Form\CustomerType', $customer);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()){
				$em = $this->getDoctrine()->getManager();
				$em->persist($customer);
				$em->flush();

				return $this->redirectToRoute('customer_show', array('id' => $customer->getId()));
			}

			return $this->render('FFCustomerBundle:customer:new.html.twig', array(
				'customer' => $customer,
				'form'     => $form->createView(),
			));
		}

		/**
		 * Creates a new Customer entity
		 *
		 * @Route("/login", name="ff_customer_login")
		 * */

		public function loginAction(Request $request){
			$authenticationUtils = $this->get('security.authentication_utils');

			//get the login error if there is one
			$error = $authenticationUtils->getLastAuthenticationError();

			//last username entered by the user
			$lastUsername = $authenticationUtils->getLastUsername();

			return $this->render('FFCustomerBundle:Default/customer:login.html.twig', array(
				//last username entered by the user
				'last_username' => $lastUsername,
				'error'         => $error,
			));
		}

		/**
		 * Displays a form to edit an existing customer entity.
		 *
		 * @Route("/{id}/edit", name="customer_edit")
		 * @Method({"GET", "POST"})
		 */
		public function editAction(Request $request, Customer $customer){
			$deleteForm = $this->createDeleteForm($customer);
			$editForm = $this->createForm('FF\CustomerBundle\Form\CustomerType', $customer);
			$editForm->handleRequest($request);

			if ($editForm->isSubmitted() && $editForm->isValid()){
				$this->getDoctrine()->getManager()->flush();

				return $this->redirectToRoute('customer_edit', array('id' => $customer->getId()));
			}

			return $this->render('FFCustomerBundle:customer:edit.html.twig', array(
				'customer'    => $customer,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			));
		}


		/**
		 * Creates a form to delete a customer entity.
		 *
		 * @param Customer $customer The customer entity
		 *
		 * @return \Symfony\Component\Form\Form The form
		 */
		private function createDeleteForm(Customer $customer){
			return $this->createFormBuilder()
				->setAction($this->generateUrl('customer_delete', array('id' => $customer->getId())))
				->setMethod('DELETE')
				->getForm();
		}

		/**
		 * @Route("/register", name="ff_customer_register")
		 */
		public function registerAction(Request $request){
			//1) Build the form
			$user = new Customer();
			$form = $this->createForm(CustomerType::class, $user);

			//2) handle the submit (will only happen on POST)
			$form->handleRequest($request);
			if ($form->isSubmitted() && $form->isValid()){
				//3) Encode the password ( you could also do this via Doctrine listener)
				$password = $this->get('security.password_encoder')->encodePassword($user, $user->getPassword());
				$user->setPassword($password);

				//4) save the User!
				$em = $this->getDoctrine()->getManager();
				$em->persist($user);
				$em->flush();

				// ... do any other work - like sending them an email, etc maybe set a "flash" success message for the user
				return $this->redirectToRoute('customer_account');
			}

			return $this->render(
				'FFCustomerBundle:customer:register.html.twig', array('form' => $form->createView())
			);
		}

		/**
		 * Finds and displays a Customer entity
		 *
		 * @Route("/account", name="customer_account")
		 * @Method({"GET", "POST"})
		 * */

		public function accountAction(Request $request){
			if (!$this->get('security.authorization_checker')->isGranted('ROLE_USER')){
				throw $this->createAccessDeniedException();
			}

			if ($customer = $this->getUser()){

				$editForm = $this->createForm('FF\CustomerBundle\Form\CustomerType', $customer, array('action' => $this->generateUrl('customer_account')));
				$editForm->handleRequest($request);

				if ($editForm->isSubmitted() && $editForm->isValid()){
					$em = $this->getDoctrine()->getManager();
					$em->persist($customer);
					$em->flush();

					$this->addFlash('success', 'Account updated.');

					return $this->redirectToRoute('customer_account');
				}

				return $this->render('FFCustomerBundle:Default/customer:account.html.twig', array(
					'customer'        => $customer,
					'form'            => $editForm->createView(),
					'customer_orders' => $this->get('ff_customer.customer_orders')->getOrders(),
				));
			} else{
				$this->addFlash('notice', 'Only logged in customers can access account page.');

				return $this->redirectToRoute('ff_customer_login');
			}

		}

		/**
		 * @Route("/logout", name="customer_logout")
		 * */
		public function logoutAction(){

		}

		/**
		 * @Route("/forgotten_password", name="customer_forgotten_password")
		 * @Method({"GET", "POST"})
		 */
		public function forgottenPasswordAction(Request $request){
			//Build a form, with validation rules  in place
			$form = $this->createFormBuilder()->add('email', EmailType::class, array('constraints' => new Email(),
			))->add('save', SubmitType::class, array(
				'label' => 'Reset!',
				'attr'  => array('class' => 'button'),
			))->getForm();

			//Check if this is a POST type request and if so, handle form
			if ($request->isMethod('POST')){
				$form->handleRequest($request);

				if ($form->isSubmitted() && $form->isValid()){
					$this->addFlash('success', 'Please check your email for reset password.');

					//todo: Send an email out to website admin or something...

					return $this->redirect($this->generateUrl('ff_customer_login'));
				}
			}

			return $this->render('FFCustomerBundle:customer:forgotten_password.html.twig', array('form' => $form->createView()));
		}


		/**
		 * Finds and displays a customer entity.
		 *
		 * @Route("/{id}", name="customer_show")
		 * @Method("GET")
		 */
		public function showAction(Customer $customer){
			$deleteForm = $this->createDeleteForm($customer);

			return $this->render('FFCustomerBundle:customer:show.html.twig', array(
				'customer'    => $customer,
				'delete_form' => $deleteForm->createView(),
			));
		}

		/**
		 * Deletes a customer entity.
		 *
		 * @Route("/{id}", name="customer_delete")
		 * @Method("DELETE")
		 */
		public function deleteAction(Request $request, Customer $customer){
			$form = $this->createDeleteForm($customer);
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()){
				$em = $this->getDoctrine()->getManager();
				$em->remove($customer);
				$em->flush();
			}

			return $this->redirectToRoute('customer_index');
		}
	}
