<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 01.09.2017
 * Time: 10:47
 */
namespace FF\CatalogBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container){
		//Override the core module 'category_menu' service
		$container->removeDefinition('category_menu');
		$container->setDefinition('category_menu', $container->getDefinition('ff_catalog.category_menu'));

		//Override the core module 'onsale' service
		$container->removeDefinition('onsale');
		$container->setDefinition('onsale', $container->getDefinition('ff_catalog.onsale'));
	}
}