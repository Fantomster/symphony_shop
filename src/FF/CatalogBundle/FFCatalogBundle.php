<?php

namespace FF\CatalogBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FFCatalogBundle extends Bundle
{
	public function build(ContainerBuilder $container){
		parent::build($container);
		$container->addCompilerPass(new \FF\CatalogBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass());
	}
	/*
	public function build(ContainerBuilder $container){
		parent::build($container);
		$container->addCompilerPass(new \FF\CatalogBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass());
	}*/
}
