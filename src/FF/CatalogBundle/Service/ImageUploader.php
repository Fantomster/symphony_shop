<?php
/**
 * Created by PhpStorm.
 * User: Konstantin
 * Date: 29.08.2017
 * Time: 11:57
 */

namespace FF\CatalogBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader
{
	private $targetDir;

	public function __construct($targetDir){
		$this->targetDir = $targetDir;
	}

	public function upload(UploadedFile $file){
		$fileName = md5(uniqid()) . '.' . $file->guessExtension();
		$file->move($this->targetDir, $fileName);
		return $fileName;
	}

}