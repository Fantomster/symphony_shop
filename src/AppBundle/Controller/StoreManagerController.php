<?php
	/**
	 * Created by PhpStorm.
	 * User: administrator
	 * Date: 20.10.2017
	 * Time: 0:04
	 */

	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;

	class StoreManagerController extends Controller
	{
		/**
		 * @Route("/store_manager", name="store_manager")
		 */
		public function indexAction(){
			return $this->render('AppBundle:default:store_manager.html.twig');
		}
	}