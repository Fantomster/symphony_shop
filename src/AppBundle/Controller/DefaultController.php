<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:default:index.html.twig');
    }
    /**
     * @Route("/about", name="about")
     * @Method("GET")
     * */
    public function aboutAction(){
    	return $this->render('AppBundle:default:about.html.twig');
    }
    /**
     * @Route("/customer-service", name="customer_service")
     * */
    public function customerServiceAction(){
    	return $this->render('AppBundle:default:customer-service.html.twig');
    }

    /**
     * @Route("/orders-and-returns", name="orders_returns")
     */
    public function ordersAndReturnsAction(){
    	return $this->render('AppBundle:default:orders-returns.html.twig');
    }

    /**
     * @Route("/privacy-and-cookie-policy", name="privacy_cookie")
     * */
    public function privacyAndCookiePolicyAction(){
    	return $this->render('AppBundle:default:privacy-cookie.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     * @Method("GET")
     * */
    public function contactAction(Request $request){
    	//Build a form, with validation rules in place
	    $form = $this->createFormBuilder()->
	    add('name', TextType::class, array('constraints' => new Assert\NotBlank()))->
	    add('email', EmailType::class, array('constraints' => new Assert\Email()))->
	    add('message', TextareaType::class, array('constraints' => new Assert\Length(array('min'=>3))))->
	    add('save', SubmitType::class, array('label'=>'Reach Out!', 'attr'=>array('class'=>'button'),
		    ))->getForm();

	    //Check if this is a POST type request and if so, handle form
		if($request->isMethod('POST')){
			$form->handleRequest($request);

			if($form->isSubmitted() && $form->isValid()){
				$this->addFlash('success', 'Your form has been submitted. Thank you.');
				//todo: Send an email to...
				return $this->redirect($this->generateUrl('contact'));
			}
		}
		//Render "contact us" page
	    return $this->render('AppBundle:default:contact.html.twig',
		    array('form'=>$form->createView()));
    }
}
