<?php

namespace FF\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('FFCatalogBundle:Default:index.html.twig');
    }
}
